# Decision mapper

### In order to run tests:
    > sbt test
   
### In order to run application. Pay attention that you configured application.conf properly:
    > sbt run

### In order to build "fat" jar:
    > sbt assembly

### In order to run main class with "provided" dependencies from IntelliJ:
    1. Right-click src/main/scala
    2. Select Mark Directory as... > Test Sources Root
    This tells IntelliJ to treat src/main/scala as a test folder for which it adds all the dependencies tagged as provided
    to any run config (debug/run).    
    Every time you do a SBT refresh, redo these step as IntelliJ will reset the folder to a regular source folder.
    
    for more approaches see this thread: https://stackoverflow.com/questions/36437814/how-to-work-efficiently-with-sbt-spark-and-provided-dependencies/42135061
