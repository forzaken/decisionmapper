import AssemblyKeys._
import sbt.Keys._
import sbtassembly.Plugin._

name := "decision_mapper"

version := "0.1"

scalaVersion := "2.11.11"

lazy val root = project
  .in(file("."))
  .settings(
    mainClass in (Compile, assembly) := Some("com.gig.Main"),

    //dependencies
    libraryDependencies ++= Dependencies.dependecies,
    dependencyOverrides ++= Dependencies.dependeciesOverrides,

    parallelExecution in Test := false,
    fork in run := true,

    //scalac
    scalacOptions ++= Seq(
      "-Ywarn-dead-code",
      "-Ywarn-inaccessible",
      "-encoding", "utf-8"
    ),

    //assembly
    assemblySettings,
    jarName in assembly := "DecisionMapper.jar",
    test in assembly := {},
    mergeStrategy in assembly := {
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case x =>
        val oldStrategy = (mergeStrategy in assembly).value
        oldStrategy(x)
    }
)

run in Compile <<= Defaults.runTask(fullClasspath in Compile, mainClass in (Compile, run), runner in (Compile, run))
runMain in Compile <<= Defaults.runMainTask(fullClasspath in Compile, runner in (Compile, run))
