import sbt._

object Dependencies {
  private val sparkVersion = "2.3.0"
  private val scalatestVersion = "3.0.1"
  private val json4sVersion = "3.5.2"
  private val typesafeConfigVersion = "1.3.2"

  val dependecies = {
    val spark = Seq(
      "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
      "org.apache.spark" %% "spark-sql" % sparkVersion % "provided"
    )

    val scalatest = Seq(
      "org.scalatest" %% "scalatest" % scalatestVersion % Test
    )

    val typesafeConf = Seq(
      "com.typesafe" % "config" % typesafeConfigVersion
    )

    spark ++ scalatest ++ typesafeConf
  }

  val dependeciesOverrides = Set(
    "org.json4s" %% "json4s-jackson" % json4sVersion
  )
}
