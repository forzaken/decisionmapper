package com.gig

import org.apache.spark.sql.{ DataFrame, Dataset, SparkSession }

object TestUtils {
  def convertDsToDf(ds: Dataset[String])(implicit sparkSession: SparkSession): DataFrame = {
    sparkSession.read
      .option("header", "true")
      .option("quote", "\u0000") //disable any quotes parsing
      .csv(ds)
  }
}
