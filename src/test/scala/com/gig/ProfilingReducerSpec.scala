package com.gig

import java.text.SimpleDateFormat

import com.gig.reducers.ProfilingReducer
import com.gig.reducers.ProfilingReducer._
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Row
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.{ StructField, StructType }
import org.apache.spark.sql.types.{ BooleanType, DateType, IntegerType, StringType }

class ProfilingReducerSpec extends BaseSpec {

  "profiling reducer" must {
    "calculate appropriate statistic [1]" in {
      val pattern = "MM-dd-yyyy"
      val sdf = new SimpleDateFormat(pattern)
      val parsedTime1 = new java.sql.Date(sdf.parse("01-28-2018").getTime)
      val parsedTime2 = new java.sql.Date(sdf.parse("01-28-2019").getTime)

      val structFields = Seq(
        StructField("a", IntegerType, nullable = true),
        StructField("b", StringType, nullable = true),
        StructField("c", BooleanType, nullable = true),
        StructField("d", DateType, nullable = true))
      val schema = StructType(structFields)
      val ds: RDD[Row] = sparkSession.sparkContext.parallelize(
        Seq(
          new GenericRowWithSchema(Array(1, "asd", true, parsedTime1), schema),
          new GenericRowWithSchema(Array(2, "bsd", false, parsedTime2), schema),
          new GenericRowWithSchema(Array(2, "bsd", false, parsedTime2), schema)),
        numSlices = 1)
      val df = sparkSession.createDataFrame(ds, schema)

      val profilingReducer = new ProfilingReducer()
      val res = profilingReducer.reduce(df)
      res.toList shouldBe List(
        ProfilingInfo("a", Seq(ValueWithCount(2, 2), ValueWithCount(1, 1))),
        ProfilingInfo("b", Seq(ValueWithCount("bsd", 2), ValueWithCount("asd", 1))),
        ProfilingInfo("c", Seq(ValueWithCount(false, 2), ValueWithCount(true, 1))),
        ProfilingInfo("d", Seq(ValueWithCount(parsedTime2, 2), ValueWithCount(parsedTime1, 1))))
    }
  }
}
