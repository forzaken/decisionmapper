package com.gig

import java.text.SimpleDateFormat

import com.gig.TestUtils.convertDsToDf
import com.gig.processors.typecast_processor.{ CastDataType, TypeMapping, TypecastProcessor }

class TypecastProcessorSpec extends BaseSpec {

  import sparkSession.implicits._

  "typecast processor" must {
    "convert fields and schema according to mappings [1]" in {
      val ds = sparkSession.sparkContext.parallelize(
        """
          |a,"b",c,"dd"
          |"1","asd","true","01-28-2018"
          |2,asd,true,01-28-2018
        """.stripMargin.lines.toList, numSlices = 1).toDS()
      val df = convertDsToDf(ds)

      val pattern = "MM-dd-yyyy"
      val sdf = new SimpleDateFormat(pattern)
      val parsedTime = new java.sql.Date(sdf.parse("01-28-2018").getTime)

      val typeMappings = Seq(
        TypeMapping("a", "aa", CastDataType.IntegerType),
        TypeMapping("b", "bb", CastDataType.StringType),
        TypeMapping("c", "cc", CastDataType.BooleanType),
        TypeMapping("dd", "dd", CastDataType.DateType(pattern)))
      val typecastProcessor = TypecastProcessor(typeMappings)
      val res = typecastProcessor.transform(df)
      val data = res.collect()
        .map(_.getValuesMap[Any](res.columns).values.toList).toList

      data shouldBe List(
        List(1, "asd", true, parsedTime),
        List(2, "asd", true, parsedTime))
    }
  }
}
