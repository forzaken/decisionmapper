package com.gig

import com.gig.processors.EmptyValuesProcessor
import TestUtils._

class EmptyValuesProcessorSpec extends BaseSpec {

  import sparkSession.implicits._

  "empty values processor" must {
    "discard rows with empty values[1]" in {
      val ds = sparkSession.sparkContext.parallelize(
        """
        |col1,col2
        |"  ",1
        |1,1
        """.stripMargin.lines.toList, numSlices = 1).toDS()
      val df = convertDsToDf(ds)

      val res = EmptyValuesProcessor().transform(df)
        .collect()
        .map(_.getValuesMap[Any](df.columns).values.toSeq).toSeq
      res shouldBe Seq(Seq("1", "1"))
    }

    "discard rows with empty values[2]" in {
      val ds = sparkSession.sparkContext.parallelize(
        """
          |col1, col2
          |,1
          |"",3
          |1," 2"
        """.stripMargin.lines.toList, numSlices = 1).toDS()
      val df = convertDsToDf(ds)
      val res = EmptyValuesProcessor().transform(df)
        .collect()
        .map(_.getValuesMap[Any](df.columns).values.toList).toList
      res shouldBe List(List(null, "1"), List("1", "\" 2\""))
    }
  }
}
