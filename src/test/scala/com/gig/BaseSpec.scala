package com.gig

import org.apache.spark.sql.SparkSession
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpec }

trait BaseSpec extends WordSpec with BeforeAndAfterAll with Matchers {
  implicit val sparkSession: SparkSession = SparkSessionCreator.init()
}
