package com.gig.reducers

import com.gig.reducers.ProfilingReducer._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.json4s.CustomSerializer
import org.json4s.JsonAST.{ JInt, JObject, JString, JValue }

@SerialVersionUID(100L)
class ProfilingReducer extends BaseReducer[Seq[ProfilingInfo]] with Serializable {
  val merge = (seq1: Seq[ValueWithCount], seq2: Seq[ValueWithCount]) => {
    val valueToCnt = seq2.map(valWithCnt =>
      (valWithCnt.value, valWithCnt.count)).toMap
    val seq1Set = seq1.map(_.value).toSet

    seq1.foldLeft(Seq.empty[ValueWithCount]) {
      case (acc, valWithCnt) =>
        val newCnt = valueToCnt.getOrElse(valWithCnt.value, 0L)
        acc :+ valWithCnt.copy(count = valWithCnt.count + newCnt)
    } ++ seq2.filterNot(x => seq1Set.contains(x.value))
  }

  override def reduce(df: DataFrame): Seq[ProfilingInfo] = {
    val colNames = df.columns.toSeq
    val initRow = new GenericRowWithSchema(colNames.map(col => {
      ProfilingInfo(col, Seq.empty[ValueWithCount])
    }).toArray, df.schema)
    df.rdd.fold(initRow) { (accRow, row) =>
      val valuesMap = row.getValuesMap[Any](colNames)
      val seq = accRow.toSeq.map {
        case ProfilingInfo(col, uniqueValues) =>
          val newValues = Option(valuesMap(col)).map({
            case ProfilingInfo(_, values) => values
            case x => Seq(ValueWithCount(x, 1L))
          }).getOrElse(Seq.empty[ValueWithCount])

          ProfilingInfo(col, merge(newValues, uniqueValues))
      }
      new GenericRowWithSchema(seq.toArray, row.schema)
    }
      .toSeq
      .map(_.asInstanceOf[ProfilingInfo])
  }
}

object ProfilingReducer {
  case class ValueWithCount(value: Any, count: Long)
  case class ProfilingInfo(colName: String, uniqueValues: Seq[ValueWithCount])

  object ProfilingInfoFormats extends CustomSerializer[ProfilingInfo](implicit formats => (
    PartialFunction.empty[JValue, ProfilingInfo],
    {
      case ProfilingInfo(colName, uniqueValues) =>
        val fields = JObject(uniqueValues.map(x => x.value.toString -> JInt(x.count)).toList)
        JObject(List(
          "Column" -> JString(colName),
          "Unique_values" -> JInt(uniqueValues.length),
          "Values" -> fields))
    }))
}
