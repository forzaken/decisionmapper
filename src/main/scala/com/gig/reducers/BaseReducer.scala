package com.gig.reducers

import org.apache.spark.sql.DataFrame

trait BaseReducer[T] {
  def reduce(df: DataFrame): T
}
