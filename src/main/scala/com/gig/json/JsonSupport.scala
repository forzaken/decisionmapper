package com.gig.json

import org.json4s.{ Extraction, Formats }
import org.json4s.jackson.{ prettyJson, parseJson }

sealed trait JsonSupport {
  implicit class JsonParser(jsonString: String) {
    def parseAs[T: Manifest](implicit formats: Formats): T = {
      Extraction.extract[T](parseJson(jsonString))
    }
  }

  implicit class JsonWriter[T](obj: T) {
    def toPrettyJson(implicit formats: Formats): String = {
      prettyJson(Extraction.decompose(obj))
    }
  }
}

object JsonSupport extends JsonSupport
