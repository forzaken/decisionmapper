package com.gig.json

import com.gig.processors.typecast_processor.TypeMapping.TypeMappingFormats
import com.gig.reducers.ProfilingReducer.ProfilingInfoFormats
import org.json4s.{ DefaultFormats, Formats }

object JsonFormats {
  implicit val formats: Formats = DefaultFormats + TypeMappingFormats + ProfilingInfoFormats
}
