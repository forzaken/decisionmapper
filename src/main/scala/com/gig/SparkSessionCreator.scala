package com.gig

import com.typesafe.config.ConfigFactory
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

object SparkSessionCreator {
  private val config = ConfigFactory.load()
  private val logger = LoggerFactory.getLogger(getClass)

  def init(): SparkSession = {
    val appName = config.getString("spark.app-name")
    val master = config.getString("spark.master-type")

    logger.info(s"setting spark master - $master")

    val sparkSession = SparkSession
      .builder()
      .appName(appName)
      .master(master)
      .getOrCreate()

    sparkSession
  }
}
