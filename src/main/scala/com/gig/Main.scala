package com.gig

import java.io.File

import com.gig.io.{ FsType, ParsingConfig, Source }
import com.gig.processors.{ BaseProcessor, EmptyValuesProcessor }
import com.typesafe.config.ConfigFactory
import org.apache.spark.sql.SparkSession
import com.gig.json.JsonSupport._
import com.gig.processors.typecast_processor.{ TypeMapping, TypecastProcessor }
import com.gig.json.JsonFormats._
import com.gig.reducers.ProfilingReducer
import org.slf4j.LoggerFactory

object Main extends App {
  implicit val sparkSession: SparkSession = SparkSessionCreator.init()

  //load config related stuff
  val config = ConfigFactory.load()
  val logger = LoggerFactory.getLogger(getClass)
  val quoteType = config.getString("spark.parsing-config.quote-type")
  val delimiter = config.getString("spark.parsing-config.delimiter")

  val workingDir = new File("").getAbsolutePath
  logger.info(s"working dir - $workingDir")

  //load type mapping
  val typeMappings = {
    val path = config.getString("spark.input-path.typemapper")
    val content = scala.io.Source.fromFile(s"$workingDir/$path").mkString
    content.parseAs[Seq[TypeMapping]]
  }

  //prepare source, transformers
  val source = {
    val data = config.getString("spark.input-path.data")
    val parsingConfig = ParsingConfig(quoteType, delimiter)
    Source(FsType.LocalFs, s"$workingDir/$data", parsingConfig)
  }

  val processors: Seq[BaseProcessor] = Seq(
    EmptyValuesProcessor(quoteType),
    TypecastProcessor(typeMappings, quoteType))

  //perform transformations
  val initDf = source.read()
  val transformedDf = processors.foldLeft(initDf) { (df, processor) =>
    processor.transform(df)
  }

  val profilingReducer = new ProfilingReducer()
  val profilingInfo: Seq[ProfilingReducer.ProfilingInfo] = profilingReducer.reduce(transformedDf)
  val profilingResultJson = profilingInfo.toPrettyJson

  logger.info(s"profiling result - $profilingResultJson")
}
