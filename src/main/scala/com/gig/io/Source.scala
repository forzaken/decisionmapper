package com.gig.io

import org.apache.spark.sql.SparkSession

case class ParsingConfig(quoteType: String, delimiter: String)

case class Source(
  fsType: FsType,
  path: String,
  parsingConfig: ParsingConfig)(implicit sparkSession: SparkSession) {
  def read() = {
    sparkSession.read
      .format("csv")
      .option("header", "true")
      .option("quote", "\u0000") //disable any quotes
      .option("delimiter", parsingConfig.delimiter)
      .load(s"${fsType.`type`}/$path")
  }
}
