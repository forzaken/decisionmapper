package com.gig.io

sealed trait FsType {
  def `type`: String
}

object FsType {
  case object LocalFs extends FsType {
    override def `type` = "file://"
  }
}
