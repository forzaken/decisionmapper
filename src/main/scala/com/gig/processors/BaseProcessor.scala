package com.gig.processors

import org.apache.spark.sql.DataFrame

trait BaseProcessor {
  def transform(df: DataFrame): DataFrame
}
