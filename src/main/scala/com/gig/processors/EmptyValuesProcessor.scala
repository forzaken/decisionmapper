package com.gig.processors

import org.apache.spark.sql.DataFrame

case class EmptyValuesProcessor(quoteType: String = "\"") extends BaseProcessor {
  private def isEmptySpaces(str: String) = {
    str.take(1) == quoteType &&
      str.takeRight(1) == quoteType &&
      str.length == 2
  }

  override def transform(df: DataFrame) = {
    df.filter(row => {
      row.toSeq.forall({
        case x: String =>
          val trimmed = x.trim
          !isEmptySpaces(trimmed) && !isEmptySpaces(trimmed.replace(" ", ""))
        case _ =>
          true
      })
    })
  }
}
