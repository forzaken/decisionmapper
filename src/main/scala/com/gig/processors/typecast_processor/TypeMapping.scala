package com.gig.processors.typecast_processor

import org.json4s.CustomSerializer
import org.json4s.JsonAST.{ JObject, JValue }

case class TypeMapping(
  existingColName: String,
  newColName: String,
  newDataType: CastDataType)

object TypeMapping {
  //TODO error handling in case of malformed records
  object TypeMappingFormats extends CustomSerializer[TypeMapping](implicit formats => ({
    case JObject(List(("existing_col_name", ecn), ("new_col_name", ncn), ("new_data_type", ndt))) =>
      TypeMapping(
        existingColName = ecn.extract[String],
        newColName = ncn.extract[String],
        newDataType = CastDataType.fromString(ndt.extract[String], None))
    case JObject(List(
      ("existing_col_name", ecn),
      ("new_col_name", ncn),
      ("new_data_type", ndt),
      ("date_expression", de)
      )) =>
      TypeMapping(
        existingColName = ecn.extract[String],
        newColName = ncn.extract[String],
        newDataType = CastDataType.fromString(ndt.extract[String], Some(de.extract[String])))
  }, PartialFunction.empty[Any, JValue]))
}
