package com.gig.processors.typecast_processor

import com.gig.processors.BaseProcessor
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.{ StructField, StructType }
import org.apache.spark.sql.{ Column, DataFrame, Row, SparkSession }

case class TypecastProcessor(typeMappings: Seq[TypeMapping], quoteType: String = "\"")(implicit sparkSession: SparkSession) extends BaseProcessor {

  //TODO better name
  private def convertToStringAndDropQuotes(str: Any) = str match {
    case null => null
    case x: String =>
      if (x.take(1) == quoteType && x.takeRight(1) == quoteType) {
        x.drop(1).dropRight(1)
      } else {
        x
      }
    case _ =>
      throw new Exception("type cast processor should accept only nullable or string values")
  }

  private def escapedColumn(columnName: String) = {
    s"$quoteType$columnName$quoteType"
  }

  override def transform(df: DataFrame) = {
    val originalColumns = df.columns.toSeq
    val existingCols = typeMappings.map(x => {
      val col = originalColumns.find(original =>
        original == x.existingColName || original == escapedColumn(x.existingColName)).getOrElse(
        throw new Exception(s"can't find column ${x.existingColName} in $originalColumns"))
      col
    })

    val structFields = typeMappings.map(tm => {
      StructField(tm.newColName, tm.newDataType.sparkType(), nullable = true)
    })
    val schema = StructType(structFields)

    val rdd: RDD[Row] = df
      .select(existingCols.map(x => new Column(x)): _*)
      .rdd
      .map(row => {
        val valuesMap = row.getValuesMap[Any](existingCols)
        val castedFields = {
          typeMappings.zip(existingCols).map {
            case (tm, existingCol) =>
              val v = convertToStringAndDropQuotes(valuesMap(existingCol))
              tm.newDataType.cast(v)
          }
        }

        new GenericRowWithSchema(castedFields.toArray, schema)
      })

    sparkSession.createDataFrame(rdd, schema)
  }
}
