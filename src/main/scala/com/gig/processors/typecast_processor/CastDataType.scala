package com.gig.processors.typecast_processor

import java.text.SimpleDateFormat
import java.util.Date

import org.apache.spark.sql.types.DataType
import org.apache.spark.sql.types.{
  StringType => SparkStringType,
  IntegerType => SparkIntegerType,
  DateType => SparkDateType,
  BooleanType => SparkBooleanType
}

import scala.util.Try

sealed trait CastDataType {
  def cast(value: String): Any
  def sparkType(): DataType
}

object CastDataType {
  case object StringType extends CastDataType {
    override def cast(value: String) = value

    override def sparkType(): DataType = SparkStringType
  }

  case object IntegerType extends CastDataType {
    override def cast(value: String) = Try(value.toInt).getOrElse(null)

    override def sparkType() = SparkIntegerType
  }

  case class DateType(format: String) extends CastDataType {
    private val sdf = new SimpleDateFormat(format)

    override def cast(value: String) = {
      val date: Date = sdf.parse(value)
      Try(new java.sql.Date(date.getTime)).getOrElse(null)
    }

    override def sparkType() = SparkDateType
  }

  case object BooleanType extends CastDataType {
    override def cast(value: String) = {
      Try(value.toBoolean).getOrElse(null)
    }

    override def sparkType() = SparkBooleanType
  }

  //TODO open/closed principle, consider using reflection
  def fromString(str: String, format: Option[String]) = str match {
    case "string" => StringType
    case "integer" => IntegerType
    case "date" =>
      val fmt = format.getOrElse(throw new Exception("missed format for date"))
      DateType(fmt)
    case "boolean" => BooleanType
    case t => throw new Exception(s"unsupported type $t")
  }
}
